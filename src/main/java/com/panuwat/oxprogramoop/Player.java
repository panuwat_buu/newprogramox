/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panuwat.oxprogramoop;

/**
 *
 * @author AKYROS
 */
public class Player {

    private char name;
    private boolean win;
    private boolean lose;
    private boolean draw;

    public Player(char name) {
        this.name = name;
        this.win = false;
        this.lose = false;
        this.draw = false;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public boolean isLose() {
        return lose;
    }

    public void setLose(boolean lose) {
        this.lose = lose;
    }

    public boolean isDraw() {
        return draw;
    }

    public void setDraw(boolean draw) {
        this.draw = draw;
    }

    public void win() {
        win = true;
    }

    public void draw() {
        draw = true;
    }

    public void lose() {
        lose = true;
    }
}
